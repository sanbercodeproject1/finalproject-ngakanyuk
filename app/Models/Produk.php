<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Produk extends Model
{
    use HasFactory;
    protected $table = "produk";
    protected $fillable = ["name", "harga", "image","deskripsi"];


    public function transaksi()
    {
        return $this->hasMany(Transaksi::class, 'produk_id');
    }
}
