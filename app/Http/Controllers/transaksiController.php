<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\Produk;
use Auth;



class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $idUser = Auth::id();
        $title = '';
        $produk = Produk::get();
        $cart = Transaksi::where('user_id',$idUser)->get();
        return view('cart.cart' ,[
            "title"=>$title, 
            "produk" => $produk,
            "cart" => $cart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
        ]);

        $idUser = Auth::id();
        $itemproduk = Produk::find($request->produk_id);
        $cart = Transaksi::where('user_id',$idUser)->first();

        $jumlah =1;
        $cart = new Transaksi;
        $cart->jumlah = $jumlah;
        $cart->total = $itemproduk->harga;
        $cart->user_id = $idUser;
        $cart->produk_id = $itemproduk->id;
 
        $cart->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Transaksi::find($id);
        $cart->delete();
 
        return redirect('/transaksi');
    }


}
