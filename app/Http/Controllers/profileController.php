<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use Auth;


class ProfileController extends Controller
{
    public function index(){
        $title = "";

        $idUser = Auth::id();

        $profile = Profile::where('user_id', $idUser)->first();

        if(auth()->user()->is_admin != 1){
            return view('profile.profile', ['profile'=>$profile, 'title'=>$title]);
        }
        return view('home.home', ['title'=>"Home"]);
    }

    public function update(Request $request, $id){
        
        $request->validate([
            'name' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);
        $profile = Profile::find($id);
        $profile ->name = $request->name;
        $profile ->alamat = $request->alamat;
        $profile ->no_hp = $request->no_hp;
        $profile->save();


        return redirect('/profile')->withSuccess('Profile Modified Successfully!');
    }

}
