<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function indexHome() {
        return view('index.home');
    }

    public function produkProducts() {
        return view('produk.products');
    }

    public function pageAbout() {
        return view('page.about');
    }

    public function pageTestimonial() {
        return view('page.testimonial');
    }

    public function kontakContact() {
        return view('kontak.contact');
    }

    public function blogBlog() {
        return view('blog.blog');
    }

    public function keranjangCart() {
        return view('keranjang.cart');
    }

    public function profilProfileCustomer() {
        return view('profil.profileCustomer');
    }
}
