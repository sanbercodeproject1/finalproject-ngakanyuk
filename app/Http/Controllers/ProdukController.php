<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Products';
        $produk = Produk::get();
        return view('admin.products.admin' ,["title"=>$title, "produk" => $produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Products';
        $produk = Produk::get();
        return view('admin.products.add', ["title"=>$title, "produk" => $produk]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'harga'=> 'required',
            'image' => 'required|file|image|mimes:jpg,png,jpeg',
            'deskripsi' => 'required'
        ]);
        
        $filename = time() . '.' .$request->image->extension();
        $request->image->move(public_path('image'), $filename);
        
        $produk = new produk;
 
        $produk->name = $request->name;
        $produk->harga = $request->harga;
        $produk->image = $filename;
        $produk->deskripsi = $request->deskripsi;
 
        $produk->save();

        $title = 'Products';
        
        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Products';
        $produk = Produk::find($id);
        return view('admin.products.detail', ["title"=>$title, "produk" => $produk]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        return view('admin.products.edit', ["title"=>'Products', "produk" => $produk]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'harga'=> 'required',
            'image' => 'required|file|image|mimes:jpg,png,jpeg',
            'deskripsi' => 'required'
        ]);

        // $filename = time() . '.' .$request->image->extension();
        // $request->image->move(public_path('image'), $filename);
        
        $produk = Produk::find($id);
        if($request->has('image')){
            $path = 'image/';
            
            File::delete($path. $produk->image);

            $filename = time() . '.' .$request->image->extension();
            $request->image->move(public_path('image'), $filename);
            $produk->image = $filename; 
            $produk->save(); 
           
        };

        $produk->name = $request->name;
        $produk->harga = $request->harga;
        // $produk->image = $filename;


        $produk->deskripsi = $request->deskripsi;
 
        $produk->save();
        return redirect('/admin')->withSuccess('Product Modified Successfully!');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $produk = Produk::find($id);

        $path = 'image/';
        File::delete($path.$produk->image);

        $produk->delete();
 
        return redirect('/admin');
    }
}
