<?php

namespace App\Http\Controllers;
use App\Models\Produk;
use App\Models\Transaksi;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('products');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title = "Home";
        return view('home.home', ["title" => $title]);
    }

    public function about()
    {
        $title = "About";
        return view('page.about', ["title" => $title]);
    }

    public function testimonial()
    {
        $title = "Testimonial";
        return view('page.testimonial', ["title" => $title]);
    }

    public function blog()
    {
        $title = "Blog";
        return view('blog.blog', ["title" => $title]);
    }

    public function contact()
    {
        $title = "Contact";
        return view('contact.contact', ["title" => $title]);
    }

    public function products()
    {
        $title = "Products";
        $produk = Produk::all();

        if(auth()->user()->is_admin == 1){
            return view('admin.products.admin', ['title'=> $title, 'produk'=>$produk] );
        }
        return view('products.products' ,["title"=>$title, "produk" => $produk]); 
        
    }

    public function products1()
    {
        $title = "Products";
        $produk = Produk::all();
        return view('products.products' ,["title"=>$title, "produk" => $produk]);
    }

    public function detail($id)
    {
        $title = "Products";
        $produk = Produk::find($id);
        return view('products.detail', ["title"=>'Products', "produk" => $produk]);

    }     
}
