@extends('Layouts.master')

@section('content')
    <!-- about page section -->
    <section class="arrival_section">
    <div class="container">
        <div class="box">
           <div class="arrival_bg_box">
              <img src="{{ asset('/Template/images/arrival-bg.jpg')}}" alt="">
           </div>
           <div class="row sticky-top">
              <div class="col-md-6 ml-auto ">
                 <div class="heading_container remove_line_bt">
                    <h2>
                       About Us
                    </h2>
                 </div>
                 <p style="margin-top: 20px;margin-bottom: 30px;">
                    <b>Ngakan Yuk</b> making possible for customers around the world to express themselves through fashion and design in choosing a more sustainable lifestyle. We create value for customers by providing our best offerings and focusing on sustainable and profitable growth.
                 </p>
                 <div class="btn-box">
                    <a href="/product" class="btn1">Shop Now</a>
                 </div>
              </div>
           </div>
        </div>
     </div>
    </section>
     <!-- end about page section -->
@endsection