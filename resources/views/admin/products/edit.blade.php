@extends('Layouts.master')

@section('content')

<div class="container mb-4">
    <hr class="mt-4 border-bottom border-danger">
<div class="card mx-auto" style="width: 40rem;">
  <div class="card-body">
    <form action="/admin/{{$produk->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
          <label>Nama Barang</label>
          <input type="text" class="form-control"name="name" value="{{$produk->name}}">
      </div>
      @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror        
      <div class="form-group">
          <label>Harga Barang</label>
          <input type="number" class="form-control"name="harga" min="1" pattern=”[0-9]” value="{{$produk->harga}}">
      </div>
      @error('harga')
          <div class="alert alert-danger">{{ $message }} </div>
      @enderror

      
       <div class="form-group">
            <label for="images" class="form-label">Pilih foto</label>
            <input type="file" class="form-control"name="image">
        </div>
       @error('image')
           <div class="alert alert-danger">{{ $message }}</div>
       @enderror
      <div class="form-group">
          <label>Deskripsi</label>
          <textarea name="deskripsi" cols="30" rows="10" class="form-control">{{$produk->deskripsi}}</textarea>
      </div>
      @error('deskripsi')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="d-flex justify-content-center my-4">
          <button type="submit" class="btn btn-danger shadow" style="width: 200px;">Submit</button>            
      </div>                               
      
    </form>
  </div>
</div>
</div>
@endsection