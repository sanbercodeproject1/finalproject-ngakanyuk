@extends('Layouts.master')

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
 
     $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this record?`,
              text: "If you delete this, it will be gone forever.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
  
</script>
@endpush


@section('content')
     <!-- product section -->
     <section class="product_section layout_padding">
        <div class="container">
           <div class="heading_container heading_center">
              <h2>
                 Our <span>products</span>
              </h2>
           </div>
           <div class="d-flex justify-content-start">
                <div>
                    <a class="btn btn-danger px-5" href="/admin/create" role="button">Add New Product</a>
                </div>
            </div>             
           <div class="row">
                    @forelse ($produk as $item)
                    {{-- card automasi looping --}}
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="box">
                                <div class="option_container">
                                    <div class="options">
                                        <form action="{{ route('transaksi.store') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="produk_id" value={{$item->id}}>
                                            <button class="btn btn-block btn-success option1" type="submit">
                                            <i class="fa fa-shopping-cart"></i>  Add To Cart
                                            </button>
                                        </form>
                                        {{-- <a href="" class="option1">
                                            Add To Cart
                                        </a> --}}
                                        <a href="" class="option2">
                                            Buy Now
                                        </a>
                                        <a href="/admin/{{$item->id}}/edit" class="option2">
                                            Edit
                                        </a>
                                        <a href="/admin/{{$item->id}}" class="option2">
                                            Detail
                                        </a>                                                                    
                                    </div>
                                </div>
                                <div class="img-box">
                                    <img src="{{asset('image/' . $item->image)}}" alt="">
                                </div>
                                <div class="detail-box">
                                    <h5>
                                        {{$item->name}}
                                    </h5>
                                    <h6>
                                        Rp {{$item->harga}}
                                    </h6>
                                </div>
                            </div>
                            <div class="mt-4 d-flex justify-content-center">
                                <form action="/admin/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger shadow show_confirm" style="width: 200px;">Delete</button>            
                                </form>
                            </div>

                        </div>
                    @empty
                        <h2>Tidak ada data</h2>
            
                    @endforelse
                

           </div>         
        </div>
     </section>
     <!-- end product section -->
     <!-- footer section -->
@endsection
