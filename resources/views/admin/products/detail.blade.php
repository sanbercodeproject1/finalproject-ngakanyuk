@extends('Layouts.master')

@section('content')
     <!-- product section -->
     <section class="product_section layout_padding">
        <div class="container">
           <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="box">
                        <div class="img-box">
                            <img src="{{asset('image/' . $produk->image)}}" alt="">
                        </div>
                        <div class="detail-box">
                            <h5>
                                {{$produk->name}}
                            </h5>
                            <h6>
                                Rp {{$produk->harga}}
                            </h6>
                        </div>                         
                    </div>
                   
                </div>
                <div class="container my-2">
                      <h5 class="card-text">{{$produk->deskripsi}}.</h5>
                </div>
            </div>
            <div class="d-flex justify-center-content my-4">
                <a href="/admin" class="btn btn-primary" role="button" style="width: 200px;">Back</a>
            </div>

        </div>
     </section>
     <!-- end product section -->
     <!-- footer section -->
@endsection