@extends('Layouts.master')

@push('scripts')
<script src="https://cdn.tiny.cloud/1/x9i3y9t4nhmf45ivd2usvak9khfl44d1kqa6e585mkgpani7/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
<script>
   var editor_config = {
     path_absolute : "/",
     selector: 'textarea.my-editor',
     relative_urls: false,
     plugins: [
       "advlist autolink lists link image charmap print preview hr anchor pagebreak",
       "searchreplace wordcount visualblocks visualchars code fullscreen",
       "insertdatetime media nonbreaking save table directionality",
       "emoticons template paste textpattern"
     ],
     toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
     file_picker_callback : function(callback, value, meta) {
       var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
       var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
 
       var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
       if (meta.filetype == 'image') {
         cmsURL = cmsURL + "&type=Images";
       } else {
         cmsURL = cmsURL + "&type=Files";
       }
 
       tinyMCE.activeEditor.windowManager.openUrl({
         url : cmsURL,
         title : 'Filemanager',
         width : x * 0.8,
         height : y * 0.8,
         resizable : "yes",
         close_previous : "no",
         onMessage: (api, message) => {
           callback(message.content);
         }
       });
     }
   };
 
   tinymce.init(editor_config);
 </script>
@endpush

@section('content')
     <!-- slider section -->
     <section class="slider_section bg-light text-dark  pt-5">
        <div class="container ">
         <div class="heading_container heading_center ">
            <h3>Contact us</h3>
         </div>
         @if(session('status'))
        <div class="row justify-content-center">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success ! </strong>  &nbsp; {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        @endif
            <div class="row">
               <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                  <div class="full">
                     <p class="text-center">Have any question or feedback?</p>
                     <form action="/contact" method="POST" enctype="multipart/form-data>
                        @csrf
                        <fieldset>
                           <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Enter your full name" name="name" autofocus />
                           @error('name')
                           <div class="alert alert-danger">{{ $message }}</div>
                           @enderror
                           <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter your email address" name="email" autofocus/>
                           @error('email')
                           <div class="alert alert-danger">{{ $message }}</div>
                           @enderror
                           <input type="text" class="form-control @error('subject') is-invalid @enderror" placeholder="Enter subject" name="subject" autofocus/>
                           @error('subject')
                           <div class="alert alert-danger">{{ $message }}</div>
                           @enderror
                           <textarea class="form-control @error('message') is-invalid @enderror" placeholder="Enter your message" name="message"></textarea>
                           @error('message')
                           <div class="alert alert-danger">{{ $message }}</div>
                           @enderror
                           <input type="submit" value="Submit" />
                        </fieldset>
                     </form>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                  <div class="d-flex flex-row pl-5">
                     <div class="p-3"><i class="fa fa-envelope fa-3x" aria-hidden="true"></i></div>
                     <div class="p-3">
                        <b>Email</b>
                        <p>admin@ngakanyuk.com</p>
                     </div>
                  </div>

                  <div class="d-flex flex-row pl-5">
                     <div class="p-3 "><i class="fa fa-phone fa-3x" aria-hidden="true"></i></div>
                     <div class="p-3">
                        <b class="pl-2">Phone</b>
                        <p class="pl-2">+6287796690111</p>
                     </div>
                  </div>

                  <div class="d-flex flex-row pl-5">
                     <div class="p-3"><i class="fa fa-map-marker fa-3x" aria-hidden="true"></i></div>
                     <div class="p-3">
                        <b class="pl-3">Main Office</b>
                        <p class="pl-3">Jl. Ngakn Raya No.69-70, Kota Jakarta Selatan <br>12130</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
     </section>
     <!-- end slider section -->
     <!-- end why section -->
@endsection
