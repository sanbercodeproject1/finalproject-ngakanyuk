@extends('Layouts.master')

@section('content')
     <!-- product section -->
     <section class="product_section layout_padding">
        <div class="container">
           <div class="heading_container heading_center">
              <h2>
                 Our <span>products</span>
              </h2>
           </div>           
           <div class="row">
                    @forelse ($produk as $item)
                    {{-- card automasi looping --}}
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="box">
                                <div class="option_container">
                                    <div class="options">
                                        @auth
                                            <form action="{{ route('transaksi.store') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="produk_id" value={{$item->id}}>
                                            <button class="btn btn-block btn-secondary" type="submit">
                                            <i class="fa fa-shopping-cart"></i>  Add To Cart
                                            </button>
                                        </form>
                                        <a href="#" class="option2">
                                            Buy Now
                                        </a>
                                        <a href="/products/{{$item->id}}" class="option2">
                                            Detail
                                        </a>      
                                        @endauth
                                        @guest
                                        <a href="/login" class="option1"
                                        data-toggle="modal" data-target="#loginModal">{{ __('Add to Cart') }}
                                        </a>
                                        <a href="/login" class="option2"
                                        data-toggle="modal" data-target="#loginModal">{{ __('Buy Now') }}
                                        </a>
                                        <a href="/login" class="option2"
                                        data-toggle="modal" data-target="#loginModal">{{ __('Detail') }}
                                        </a>       
                                        @endguest
                                        
                                                                                                    
                                    </div>
                                </div>
                                <div class="img-box">
                                    <img src="{{asset('image/' . $item->image)}}" alt="">
                                </div>
                                <div class="detail-box">
                                    <h5>
                                        {{$item->name}}
                                    </h5>
                                    <h6>
                                        Rp {{$item->harga}}
                                    </h6>
                                </div>
                            </div>

                        </div>
                    @empty
                        <h2>Tidak ada data</h2>
            
                    @endforelse
                

           </div>         
        </div>
     </section>
     <!-- end product section -->
     <!-- footer section -->
@endsection
