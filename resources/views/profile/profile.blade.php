@extends('Layouts.master')


@section('content')
<div class="card mx-auto" style="width: 40rem;">
  <div class="card-body">
    <form action="/profile/{{ $profile->id }}" method="POST" >
      @csrf
      @method('PUT')
      <hr class="mt-4 border-bottom border-danger">
      <div class="form-group">
          <label>Username</label>
          <input type="text" class="form-control"name="username" value="{{ old('username', $profile->user->username) }}" readonly >
      </div>
      @error('username')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror        
      <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control"name="email" value="{{ old('email', $profile->user->email) }}" readonly>
      </div>
      @error('email')
          <div class="alert alert-danger">{{ $message }} </div>
      @enderror
      <div class="form-group">
          <label>Password</label>
          <input type="password" class="form-control"name="password" value="{{ old('password', $profile->user->password) }}" readonly>
      </div>
      @error('password')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <hr class="mt-4 border-bottom border-danger">
      <div class="form-group">
          <label>Nama Lengkap</label>
          <input type="text" class="form-control"name="name" value="{{ old('name', $profile->name) }}">
      </div>
      @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror        
      <div class="form-group">
          <label>Alamat</label>
          <textarea name="alamat" class="form-control">{{ old('alamat', $profile->alamat) }}</textarea>
      </div>
      @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label>No HP</label>
          <input type="number" class="form-control" name="no_hp" maxlength="14" pattern=”[0-9]” value="{{ old('no_hp', $profile->no_hp) }}">
      </div>
      @error('no_hp')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="d-flex justify-content-center my-4">
          <button type="submit" class="btn btn-danger shadow" style="width: 200px;">Change</button>            
      </div>                               
      
</form>
  </div>
</div>

@endsection