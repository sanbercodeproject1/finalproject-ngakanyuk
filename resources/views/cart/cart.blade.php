@extends('Layouts.master')



@section('content')
  <div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col mt-5">
        <p><span class="h2">Shopping Cart </span></p>

        <div class="card mb-4 border-danger">
          <div class="card-body p-4">

            {{-- card --}}
            <?php
                $total = 0;
            ?>
            @forelse ($cart as $item)
            <div class="row align-items-center my-4">
              <div class="col-md-2">
                <img src="{{asset('image/' . $item->produk->image)}}"
                  class="img-fluid" alt="Generic placeholder image">
              </div>
              <div class="col-md-2 d-flex justify-content-center">
                <div>
                  <p class="small text-muted mb-4 pb-2">Nama Produk</p>
                  <p class="lead fw-normal mb-0">{{$item->produk->name}}</p>
                </div>
              </div>
              <div class="col-md-2 d-flex justify-content-center">
                <div>
                  <p class="small text-muted mb-4 pb-2">Deskripsi</p>
                  <p class="lead fw-normal mb-0">
                    {{ Str::limit($item->produk->deskripsi, 20) }}</p>
                </div>
              </div>
              <div class="col-md-2 d-flex justify-content-center">
                <div>
                  <p class="small text-muted mb-4 pb-2">Jumlah</p>
                  <p class="lead fw-normal mb-0">{{$item->jumlah}}</p>
                </div>
              </div>
              <div class="col-md-2 d-flex justify-content-center">
                <div>
                  <p class="small text-muted mb-4 pb-2">Harga</p>
                  <p class="lead fw-normal mb-0" id="harga">Rp {{$item->produk->harga}}</p>
                </div>
              </div>
              <form action="{{ route('transaksi.destroy', $item->id) }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger mx-4 my-1">Delete</button>
              </form>
            </div>
            <hr class="mt-4 border-bottom border-danger">
              <?php
                  $total += $item->produk->harga;
              ?>

            @empty
              <h2>Tidak ada data</h2>
            @endforelse

          </div>
        </div>

        <div class="card  border-danger mb-5">
          <div class="card-body p-4">

            <div class="float-end">
              <p class="mb-0 me-5 d-flex align-items-center">
                <span class="small text-muted me-2">Order total:</span> 
                <span class="lead fw-normal"> Rp {{ $total }}    
            </span>
              </p> 
            </div>

          </div>
        </div>

        <div class="d-flex justify-content-end mb-5">
          <button type="button" class="btn btn-light btn-lg mx-2">Continue shopping</button>
          <button type="button" class="btn btn-primary btn-lg mx-2">Buy now</button>
        </div>

      </div>
    </div>
  </div>
@endsection