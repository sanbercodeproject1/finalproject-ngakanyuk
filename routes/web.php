<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TransaksiController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Home
Route::get('/', [HomeController::class, 'index']);
Route::get('/about', [HomeController::class, 'about']);
Route::get('/testimonial', [HomeController::class, 'testimonial']);
Route::get('/blog', [HomeController::class, 'blog']);
Route::get('/contact', [HomeController::class, 'contact']);
Route::get('/products', [HomeController::class, 'products'])->name('admin.home')->middleware('is_admin');
Route::get('/products/{item_id}', [HomeController::class, 'detail']);
Route::get('/products1', [HomeController::class, 'products1']);


//CRUD Profile
Route::resource('profile', ProfileController::class)->only('index', 'update');

//CRUD Product
Route::resource('admin', ProdukController::class);

//CRUD Cart
Route::resource('transaksi', TransaksiController::class);


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});


